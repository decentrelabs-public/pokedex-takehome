"use client";

/**
 * Table component to display fetched pokemon data.
 * Candidates are to fill into neccessary functions and
 * table components.
 */

/* Matarial UI Components */
import Button from "@mui/material/Button";
import Stack from "@mui/material/Stack";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";

import { useState } from "react";

/**
 * Function to extract pokemon number from URL .
 *
 * @param url pokemon url string
 * @returns pokemon number
 */
function getNumber(url) {
  if (!url) {
    return "error";
  }

  /* ADD CODE HERE */
}

/**
 * Table component to display data
 */
export default function PokemonTable(props) {
  const [data, setData] = useState(props.data || []);

  /**
   * Click handler function to sorts the pokemon by name.
   */
  const sortByNameClickHandler = () => {
    let dataSortedByName = [];

    /* ADD CODE HERE */

    /* Set state after data is sorted */
    setData([...dataSortedByName]);
  };

  /**
   * Click handler function to sorts the pokemon by number.
   */
  const sortByNumberClickHandler = () => {
    let dataSortedByNumber = [];

    /* ADD CODE HERE */

    /* Set state after data is sorted */
    setData([...dataSortedByNumber]);
  };

  return (
    <>
      <Stack spacing={2} direction="row">
        <Button variant="outlined" onClick={() => sortByNameClickHandler()}>
          Sort Alphabetically
        </Button>
        <Button variant="outlined" onClick={() => sortByNumberClickHandler()}>
          Sort Numerically
        </Button>
      </Stack>
      <TableContainer component={Paper}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Name</TableCell>
              <TableCell>Url</TableCell>
              <TableCell>Number</TableCell>

              {/* ADD EXTRA COLUMNS HERE */}
            </TableRow>
          </TableHead>
          <TableBody>
            {data.map((elem) => (
              <TableRow key={elem.name}>
                {/* FILL IN TABLE */}
                <TableCell></TableCell>
                <TableCell></TableCell>
                <TableCell>{getNumber()}</TableCell>

                {/* ADD EXTRA CELLS HERE */}
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
}
