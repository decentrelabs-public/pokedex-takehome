/**
 * Root page for the Pokedex web application.
 * Candates are to fill in specified functions and
 * add necessary component(s).
 *
 * Please read comments carefully.
 */

/* CSS Libraries -- do not remove */
import CssBaseline from "@mui/material/CssBaseline";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";

/* Components */
import PokemonTable from "../components/PokemonTable";

/**
 * Function to fetch data from pokemon api.
 * Use the URL "https://pokeapi.co/api/v2/pokemon?limit=151"
 *
 * @returns JSON
 */
async function fetchData() {
  /* ADD CODE HERE */
}

/**
 * Root component for application.
 */
export default async function Home() {
  /* variable to fetch and store data */
  const data = await fetchData();

  return (
    <main>
      {/* Do not remove */}
      <CssBaseline />
      <Container>
        <Typography variant="h2" align="center" gutterBottom>
          DecentreLabs Pokedex
        </Typography>

        {/* 
          ADD PokemonTable COMPONENT HERE.
          You should pass the fetched data as prop called 'data' to the component. 
        */}
      </Container>
    </main>
  );
}
