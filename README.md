## Pokédex Take Home Project
Thank you for applying for the Software Engineering Intern position at DecentreLabs. This take home project is a web based project using the [Next.js](https://nextjs.org/) and [Material UI](https://mui.com/material-ui/getting-started/), and is intended to assess the current skill level of the candidates. With that said, we do not expect candidates to be React or Next.js wizards for this project and have provided a template for you to fill in code where necessary. The tasks in this project may be similar to tasks that you can expect to encounter as an intern. 

Please follow the instructions below. Also, please feel free to send an email to paustria@decentrelabs.com if you have any questions during this process.

## Submission Instructions
You will have **1 week** after being notified to complete the take home project, but we do not to expect you to have to spend the entire week on it (a few hours or so should be sufficient). Once you accomplish the project's requirements to the best of your abilities, please follow these steps:

- delete the `node_modules` folder
- **zip** the project folder and name the folder according to the following format: `firstname_lastname_pokedex_takehome.zip`
- upload the zipped folder to this Google [form](https://forms.gle/nmBeEAZKqNxGVXs96)

## Project Description
Create a simple Pokemon Pokédex that displays information about the original 151 Pokemon. You are also provided with a [user story](https://www.atlassian.com/agile/project-management/user-stories) (detailed further in this README file) and have been given instructions to further expand the application in order to make it more informational, valuable, and appealing to end users. Pokemon data is provided by an open source API located at [PokéAPI](https://pokeapi.co/) and is returned in JSON format. The most basic example of what the Pokédex should look like can be seen [here](https://pokedex-demo-kappa.vercel.app/).

### Project Requirements
At a minimum, the table should display the **first 151 Pokemon** with at least 3 headers: **Name**, **Url** and **Number**.  It should also include two sorting capabilities: 1) **sort alphabetically** and 2) **sort numerically**

- in `src/app/page.js`,  complete **fetchData()**  function to get the data. Use the following url: [https://pokeapi.co/api/v2/pokemon?limit=151](https://pokeapi.co/api/v2/pokemon?limit=151) 
- pass the data as a prop to the `PokemonTable` component located at `src/components/PokemonTable.js`
- fill in the table elements to display the information
- fill in the **getNumber()** function to extract the Pokemon number from the url
- fill in the **sortByNameClickHandler()** function which executes when SORT ALPHABETICALLY is clicked.
- fill in the **sortByNumberClickHandler()** function which executes when SORT NUMERICALLY is clicked.

In addition to the above, and **based on the user story below**, add a feature to your application beyond the above requirements that would address the user story. Use your imagination and create a feature you believe would best solve the user needs. In addition, we encourage you to style and decorate the application to your liking.

### User Story
`As a Pokémon enthusiast, I want to access comprehensive information about different Pokémon species, such as their abilities, types, and evolutionary chains. This will help me strategize my Pokémon team and enhance my gaming experience.`

## Getting Started
Before running the project, you will need to have the following pre-installed:
- [git](https://git-scm.com/)
- [node](https://nodejs.org/en) 
- npm (should be installed along with node)

Next, follow the instructions to run the development server:
```bash
# clone the repository. 
git clone https://gitlab.com/decentrelabs-public/pokedex-takehome.git

# go into the project
cd pokedex-takehome

# install project depencencies
npm install

# run the dev server
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the web application.

You can start editing the page by modifying `src/app/page.js`. The dev server auto-updates as you edit and save the file. You will be working on this project on your local machine and can use any IDE or text editor you choose (e.g. vscode, atom, vim..etc).

## Resources
Below are some resources to help you complete the project. Additionally, you may use any other resource(s) to complete the project.

- fetching data in Next.js [link](https://nextjs.org/docs/app/building-your-application/data-fetching/fetching-caching-and-revalidating#fetching-data-on-the-server-with-fetch)
- passing props into components [link](https://nextjs.org/learn/react-foundations/displaying-data-with-props)
- material UI basic table API [link](https://mui.com/material-ui/react-table/#basic-table)
